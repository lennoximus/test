/* Connect to a JSON Data Source */
const HOST = 'http://178.128.196.163:3000';

export function getRecords() {
    return fetch(`${ HOST }/api/records`, {
        method: 'GET'
    }).then((res) => res.json());
}

export function saveRecords(params) {
    const headers = new Headers({
        'Content-Type': 'application/json',
    });

    return fetch(`${ HOST }/api/records`, {
        method: 'PUT',
        headers,
        body: JSON.stringify(params),
    }).then((res) => res.json());
}

export function deleteRecords(id) {
    return fetch(`${ HOST }/api/records/${ id }`, {
        method: 'DELETE',
    }).then((res) => res.json());
}

export function postRecords(id, params) {
    const headers = new Headers({
        'Content-Type': 'application/json',
    });

    return fetch(`${ HOST }/api/records/${ id }`, {
        method: 'POST',
        headers,
        body: JSON.stringify(params),
    }).then((res) => res.json());
}