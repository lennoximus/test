/* Component add row */
import React, { Component } from 'react';

class Header extends Component {
    state = {
        db: '',
        name: '',
        surname: '',
    }

    onInput(type, event) {
        this.setState({
            [type]: event.target.value,
        });
    }

    clearData() {
        this.setState({
            db: '',
            name: '',
            surname: '',
        });
    }

    render() {
        const { db, name, surname } = this.state;

        return (
            <tr>
                <th><input type="text" value={ db } onInput={this.onInput.bind(this, 'db')} /></th>
                <th><input type="text" value={ name } onInput={this.onInput.bind(this, 'name')} /></th>
                <th><input type="text" value={ surname } onInput={this.onInput.bind(this, 'surname')} /></th>
                <th colSpan="2">
                    <button
                        className="button-style"
                        onClick={() => {
                            this.props.onSave(this.state);
                            this.clearData();
                        }}
                    >Добавить запись</button>
                </th>
            </tr>
        );
    }
}

export default Header;