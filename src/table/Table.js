/* render component table */
import React, { Component } from 'react';
import { getRecords, saveRecords } from '../data/api';
import { deleteRecords } from '../data/api';
import Row from './Row';
import Header from './Header';
import './Table.css';
import { postRecords } from '../data/api';


class Table extends Component {
    state = {
        data: [],
        editId: null,
    }
    
/* /api/records - GET */
    async getData() {
        const response = await getRecords();

        await this.setState({
            data: response,
        });
    }

/* /api/records - DELETE */
    async delData(id) {
        await deleteRecords(id);
        this.getData();
    }

/* /api/records - PUT */
    async onSave(data) {
        const params = {
            data,
        };

        await saveRecords(params);
        this.getData();
    }

/* /api/records - POST */
    onSelected(id) {
        this.setState({
            editId: id,
        });
    }

/* button - update/save */
    async onSelectedGet(id, data) {
        const params = {
            data,
        }

        await postRecords(id, params);

        this.setState({
            editId: null,
        });
        this.getData();        
    }

    componentDidMount() {
        this.getData();
    }

    render() {
        return (
            <table>
                <thead>
                    <Header onSave={this.onSave.bind(this)} />
                </thead>
                <tbody>
                { this.state.data.map(row => row.data ?
                    <Row
                        data={row}
                        delData={this.delData.bind(this)}
                        onSelected={this.onSelected.bind(this)}
                        onSelectedGet={this.onSelectedGet.bind(this)}
                        isEdit={this.state.editId === row._id}
                        key={row._id}
                    />
                    : null)
                }
                </tbody>
            </table>
        );
    }
}

export default Table;